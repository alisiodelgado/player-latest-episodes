import React from 'react';
import Header from '../components/ui/Header'
import Cards from '../components/ui/Cards'
import CardMedia from '../components/ui/CardMedia'
import LatestEpisodes from '../components/ui/LatestEpisodes'
import ReactJkMusicPlayer from 'react-jinke-music-player'


function App() {
  return (
    <div className="App">


<CardMedia/>
<ReactJkMusicPlayer/>
    </div>
  );
}

export default App;
